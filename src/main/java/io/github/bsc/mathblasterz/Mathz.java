package io.github.bsc.mathblasterz;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Mathz {
    static final String answer(String question) {
        question = question.replace("^", " ^ ");
        String[] parts = question.split(" ");
        if ("?".equals(parts[0])) {
            parts =  questionmarkOnLeft(parts);
        }
        if ("?".equals(parts[2])) {
            parts =  questionmarkOnMiddle(parts);
        }
        double answer = 2;
        try {
            Double x = Double.valueOf(parts[0]);
            Double y = Double.valueOf(parts[2]);

            switch (parts[1]) {
                case "-": {
                    answer = x - y;
                    break;
                }
                case "+": {
                    answer = x + y;
                    break;
                }
                case "x": {
                    answer = x * y;
                    break;
                }
                case "%": {
                    return divideDoubles(x, y);
                }
                case "^": {
                    answer = Math.pow(x, y);
                }
//            default : {
//                throw new UnsupportedOperationException(question);
//            }
            }
            return Double.toString(answer);
        } catch(NumberFormatException e) {
            return "UNKNOWN";
        }
    }

    Map<String,String> inverses = new HashMap<>();

    {
        inverses.put("+", "-");
        inverses.put("-", "+");
    }

    static String[] questionmarkOnLeft(String[] parts) {
        // TODO: 2020-11-09
        //  The re-ordering of the arguments needs to be rechecked
        // "? + -1 = -24"
        // [ "?", "+", "-1", "=", "-24" ]
        // [ "-24", "-" , "-1" , "=" , "?"]

        Map<String,String> inverses = new HashMap<>();

        {
            inverses.put("+", "-");
            inverses.put("-", "+");
        }

        return new String [] {
                parts[4],
                inverses.get(parts[1]),
                parts[2],
                parts[3],
                parts[0]
        };
    }

    static String[] questionmarkOnMiddle(String[] parts) {
        // TODO: 2020-11-09
        //  The re-ordering of the arguments needs to be rechecked
        // "5 + ? = -68"
        // [ "?", "+", "-1", "=", "-24" ]
        // [ "-24", "-" , "-1" , "=" , "?"]

        // 89 - ? = 67
        // 89 - 67 = ?

        Map<String,String> inverses = new HashMap<>();

        {
            inverses.put("+", "-");
            inverses.put("-", "-");
        }

        return new String [] {
                parts[0],
                inverses.get(parts[1]),
                parts[4],
                parts[3],
                parts[2]
        };
    }

    static String divideInts(Integer x, Integer y) {
        double floatAnswer = (1.0 * x)/ y;
        return String.format("%1.3f", floatAnswer);
    }

    static String divideDoubles(Double x, Double y) {
        double floatAnswer = x / y;
        return String.format("%1.3f", floatAnswer);
    }

    public static void main(String[] args) throws IOException {
        String TEAM_ID = System.getenv("MATH_TEAM_ID");

        String state = Schema.State.READY;

        while (Schema.State.running(state)) {
            String questionAsJson = JavaNet.POST.responseBody(
                    API.MATHZ,
                    API.Question.requestBody(TEAM_ID)
            );

            String answer = answer(
                    Schema.question(
                            questionAsJson
                    )
            );

            String answerAsJson = JavaNet.POST.responseBody(
                    API.MATHZ,
                    API.Answer.requestBody(TEAM_ID, answer)
            );

            System.out.println(
                    FakeJson.prettyPrint(answerAsJson)
            );


            state = Schema.state(answerAsJson);
        }
    }

    static class API {
        static final String MATHZ = "http://mathz-blastererz.org/api/mathz";

        static class Question {
            static String requestBody(String teamId) {
                return FakeJson.object(
                        Collections.singletonMap(
                                Schema.ACCOUNT_ID,
                                teamId
                        )
                );
            }
        }

        static class Answer {
            static String requestBody(String teamId, String answer) {
                Map<String,String> data = new HashMap<>();
                data.put(Schema.ACCOUNT_ID, teamId);
                data.put(Schema.ANSWER, answer);

                return FakeJson.object(data);
            }
        }
    }

    static class Schema {
        static final String ACCOUNT_ID = "account_uuid";
        static final String ANSWER = "answer";

        static String state(String responseBody) {
            // TODO: this should be done with a real json parser
            return match(
                    Patterns.state,
                    responseBody
            );
        }

        private static String match(Pattern p, String responseBody) {
            Matcher matcher = p.matcher(responseBody);
            if (matcher.find()) {
                return matcher.group(1);
            }
            throw new AssertionError(responseBody);
        }


        static String question(String responseBody) {
            return match(
                    Patterns.question,
                    responseBody
            );
        }

        static class Patterns {
            static final Pattern state = Pattern.compile("\"state\":\"([^\"]+)\"");
            static final Pattern question = Pattern.compile("\"question\":\"([^\"]+)\"");
        }

        @SuppressWarnings("unused")
        static class State {
            static final String READY = "Ready";
            static final String INCORRECT = "Incorrect!";
            static final String IN_PROGRESS = "In Progress";
            static final String CORRECT = "Correct!";
            static final String TIMED_OUT = "Timed Out!";
            static final String WINNER = "A Winner Is You!";

            static final List<String> RunningStates = Arrays.asList(
                    State.READY,
                    State.CORRECT,
                    State.IN_PROGRESS
            );

            static boolean running(String state) {
                return RunningStates.contains(state);
            }
        }
    }
}
