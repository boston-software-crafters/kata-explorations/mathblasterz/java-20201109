package io.github.bsc.mathblasterz;

/**
 * @author Danil Suits (danil@vast.com)
 */
class TDD {
    static void tdd(
            String question,
            String expected
    ) {
        if (!Mathz.answer(question).equals(expected)) {
            throw new AssertionError(
                    question + " => " +
                            Mathz.answer(question)
            );
        }
    }

    public static void main(String[] args) {
        try {
            tdd("0 + 7 = ?", "7.0");
            tdd("58 - 62 = ?", "-4.0");
            tdd("15 x 31 = ?", "465.0");
            tdd("74 % 70 = ?", "1.057");
            tdd("23^6 = ?", "1.48035889E8");
            tdd("98.89 % 87.384 = ?", "1.132");
            tdd("? + -1 = -24", "-23.0");
            tdd("5 + ? = -68", "73.0");
            tdd("89 - ? = 67", "22.0");
            tdd("22^8 = ?", "5.4875873536E10");
            System.out.println("PASSED");
        } catch (AssertionError e) {
            System.out.println(e.getMessage());
            System.out.println("FAILED");
        }
    }
}
