package io.github.bsc.mathblasterz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

class JavaNet {
    static class POST {
        static String responseBody(String uri, String requestBody) throws IOException {
            URL url = new URL(uri);
            URLConnection con = url.openConnection();
            HttpURLConnection http = (HttpURLConnection) con;
            http.setRequestMethod("POST"); // PUT is another valid option
            http.setDoOutput(true);

            byte[] out = requestBody.getBytes(StandardCharsets.UTF_8);
            int length = out.length;

            http.setFixedLengthStreamingMode(length);
            http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.connect();

            try (OutputStream os = http.getOutputStream()) {
                os.write(out);
            }

            StringBuilder responseBody = new StringBuilder();
            try (Reader reader = reader(http.getInputStream())) {
                int c = 0;
                while ((c = reader.read()) != -1) {
                    responseBody.append((char) c);
                }
            }
            return responseBody.toString();
        }

        static Reader reader(InputStream is) {
            return new BufferedReader(
                    new InputStreamReader(
                            is,
                            StandardCharsets.UTF_8
                    )
            );
        }
    }
}
